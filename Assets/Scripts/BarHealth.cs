﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarHealth: MonoBehaviour
{
    public Image bar;
    public float startingX;
    public float startingY;
    public int tempVariable = 1;
    public PlayerController playerController;

    // Start is called before the first frame update
    void Start()
    {
        Vector2 barSize = bar.rectTransform.sizeDelta;
        startingX = barSize.x;
        startingY = barSize.y;

    }

    // Update is called once per frame
    void Update()
    {
        bar.rectTransform.sizeDelta = new Vector2(playerController.health/5, startingY);
    }
}
