﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "EquipmentObject", menuName = "EquipmentObject/Equipment", order = 1)]
public class EquipmentObject : ScriptableObject
{
    public bool isPassive; //Does this item have an effect all the time (true) or when held (false).
    public bool limitedCharges; //Can you only use the item once?

    public int charges; //How many times can you use it?

    public int passiveHealth; //Health change per second
    public int passiveMorale; //Morale change per second
    public int passiveCorruption; //Corruption change per second

    public int useHealth; //Health change on use
    public int useMorale; //Morale change on use
    public int useCorruption; //Corruption change on use

    public int damageInflicted; //Damage given to enemies.

}
