﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public int health = 1000; //Variables here could be changed.
    public int morale = 1000;
    public int corruption = 0;
    public int corruptionRate = 5;
    public int moraleRate = 20;

    public float tickTime = 1.0f;
    public float lastTick = 0.0f;

    public bool alive = true;
    public bool atShrine = false;
    public bool inCorruptionZone = false;

    public Shrine currentShrine;


    void ProcessMorale() //No check for time as this is processed regularly by Update()
    {
    if (morale>990)
        {
            corruption -= corruptionRate * 20;
		}
    else if (morale>900)
        {
            corruption -= corruptionRate * 5;
		}
    else if (morale>750)
        {
            corruption -= corruptionRate * 2;
        }
    else if (morale>500)
        {
            corruption -= corruptionRate;
		}
    if (corruption<0)
        {
            corruption = 0;
		}
    if (morale>500)
        {
            morale -= moraleRate;
		}
	}


    void ProcessDeath()
    {
        //Swap character state to dead. This should prevent movement, being attacked, or AI targetting the player.
        alive = false;
        //Swap back to alive after 5 seconds.
        StartCoroutine("Resurrect");        
    }

    IEnumerator Resurrect()
    {
        yield return new WaitForSeconds(5);
        health = 1000;
        ModifyCorruption(200);
        alive = true;
    }

    void processCorruptionDefeat()
    {
        corruption = 0; //End the game here. You've become too evil. This should probably give a defeat screen with a menu to return to the start.
        UnityEngine.Debug.Log("Game would be lost here.");
	}

    public void ModifyHealth(int healthChange) //Pass positive for healing or negative for damage.
    {
        if (alive == true)
        {
            health = health + healthChange;
            if (health > 1000) { health = 1000; }
            if (health <= 0) { ProcessDeath(); }
        }
    }

    public void ModifyMorale(int moraleChange)
    {
        morale = morale + moraleChange;
        if (morale > 1000) { morale = 1000; }
        if (morale < 0) { morale = 0; }
    }

    public void ModifyCorruption(int corruptionChange)
    {
        corruption = corruption + corruptionChange;
        if (corruption > 1000) { processCorruptionDefeat(); }
        if (corruption < 0) { corruption = 0; }
    }




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time>(lastTick+tickTime))
        {
            lastTick = lastTick+tickTime;
            ProcessMorale();
            if (inCorruptionZone) { ModifyCorruption(40); }
		}

        if (Input.GetKeyDown(KeyCode.E) && atShrine) // both conditions can be in the same branch
        {
            if (currentShrine.shrineUsed == false) {
                ModifyMorale(500);
                currentShrine.shrineUsed = true;
            }
        }
    }
}
