﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Debug = UnityEngine.Debug;


public class KriegMovement : MonoBehaviour
{
    public float jumpHeight;
    private bool isJumping = false; 
    public const float speed = 20f;
    public float moveSpeed = 5f;
    private float betweenAttack;
    public float attackStart;
    public Transform attackPos;
    public float attackRange;
    public LayerMask enemyDetector;
    public int damage;
    private Rigidbody2D playerRigidbody2D;

    private void Awake()
    {
        playerRigidbody2D = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        transform.position += movement * Time.deltaTime * moveSpeed;
        
            
            if (Input.GetKeyDown(KeyCode.Space) && !isJumping) 
        {
            playerRigidbody2D.AddForce(Vector2.up * jumpHeight); 
            isJumping = true;
            UnityEngine.Debug.Log("Space");
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            gameObject.GetComponent<Collider2D>().enabled = false;
            Invoke("Jumper", 0.5f);
            UnityEngine.Debug.Log("Space");
        }
        if (betweenAttack <= 0)
        {
            if (Input.GetKey(KeyCode.E))
            {
                UnityEngine.Debug.Log("attack");
                Collider2D[] enemiesToHit = Physics2D.OverlapCircleAll(attackPos.position, attackRange, enemyDetector);
                for (int i = 0; i < enemiesToHit.Length; i++)
                {
                    enemiesToHit[i].GetComponent<EnemyDamege>().TakeDamege(damage);
                }
            }
            betweenAttack = attackStart;
        }
        else
        {
            betweenAttack -= Time.deltaTime;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
       if (collision.gameObject.name == "TestFloor" )
       {
           isJumping = false;
       }     
    }
    public void Jumper()
    {
        gameObject.GetComponent<Collider2D>().enabled = true;
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
}
// to help integrate the code you will need  to make a empty object and call it attackPos this will be the starting attack position
// also the damege for some reason is doubled what its meant to so 5 =10 damage and 10 = 20
// everything that needs to be tweaked is in the imspector as opposed to fucking around with the code
// the code can easily be repeated for a variety of attacks IE bottom attacks and power attacks although to stop the code being massive you may want to split combat and movement then
// the enemies will need 2 collisders one to be hit by the player and another to keep it from falling through the level. this is only a quick fix though
// and finally enemies will have to be on the ememies layer for them to be targeted this doesnt seem to impaact the leves



